import nn as NeuralNetwork
import numpy as np
import os

import random

clear = lambda: os.system('clear')
clear()

def createCandidate(x):
    c = x + np.random.randn(1)
    return c

(xs, ys) = NeuralNetwork.mnist_to_data('../data/', 'mnist_train.csv')
neuralNetwork = NeuralNetwork.PerceptronMulticouche(784, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
bestParameter = neuralNetwork.get_params()
currentParameter = bestParameter

bestError = NeuralNetwork.erreur_quadratique(xs, ys, neuralNetwork)
bestPrecision = NeuralNetwork.precision(xs, ys, neuralNetwork)

print("Precision :", str(bestPrecision))
print("Erreur quadratique :", str(bestError))
print("\nIs running, please wait....\n")

for i in range(0, 100):
    bestParameter = neuralNetwork.get_params()
    for i in range(0, len(bestParameter)):
        currentParameter[i] = createCandidate(bestParameter[i])
    neuralNetwork.set_params(currentParameter)
    currentError = NeuralNetwork.erreur_quadratique(xs, ys, neuralNetwork)
    currentPrecision = NeuralNetwork.precision(xs, ys, neuralNetwork)
    if (currentError < bestError) and (currentPrecision > bestPrecision):
        bestError = currentError
        bestPrecision = currentPrecision
        bestParameter = currentParameter
    neuralNetwork.set_params(bestParameter)

print("Precision :", str(bestPrecision))
print("Erreur quadratique :", str(bestError))