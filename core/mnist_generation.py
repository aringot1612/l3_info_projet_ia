import jeulib as Jeu;
import joueurlib
import interfacelib

import sys
import numpy as np
import time
import datetime

filename = "../data/data.csv"

def initPlayers(j, typeIA1, optIA1, typeIA2, optIA2):
    if typeIA1 == "MinMax" :
        blackPlayer = joueurlib.IA_MinMax(j, "noir", {"Profondeur":optIA1})
    elif typeIA1 == "AlphaBeta" :
        blackPlayer = joueurlib.IA_AlphaBeta(j, "noir", {"Profondeur":optIA1})
    else:
        blackPlayer = joueurlib.IA_Random(j, "noir")

    if typeIA2 == "MinMax" :
        whitePlayer = joueurlib.IA_MinMax(j, "blanc", {"Profondeur":optIA2})
    elif typeIA2 == "AlphaBeta" :
        whitePlayer = joueurlib.IA_AlphaBeta(j, "blanc", {"Profondeur":optIA2})
    else:
        whitePlayer = joueurlib.IA_Random(j, "blanc")
    return [blackPlayer, whitePlayer]

def autorun(occurences):
    myfile = open(filename, 'w')
    for i in range(0, occurences):
        j = Jeu.Jeu(8, {"interface":False, "choix_joueurs":False})
        (black, white) = initPlayers(j, "rnd", 0, "rnd", 0)
        j.noir = black
        j.blanc = white
        j.joueur_courant = j.noir
        while not j.partie_finie:
            coup = j.joueur_courant.demande_coup()
            j.jouer(coup)
            myfile.write(str(j.noir.EvalBoard(j.plateau, 1)) + ',' + ','.join(map(lambda b: ','.join(map(str, b)), j.plateau.tableau_cases))+'\n')
    myfile.close()

autorun(1000)