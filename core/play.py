import jeulib as Jeu 
import joueurlib
import interfacelib

import numpy as np
import time
import datetime

j = Jeu.Jeu(8, {"choix_joueurs":False})
j.noir = joueurlib.Humain(j, "noir")
j.blanc = joueurlib.IA_AlphaBeta(j, "blanc", {"Profondeur":5})
j.joueur_courant = j.noir
j.demarrer()