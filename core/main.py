import jeulib as Jeu 
import joueurlib
import interfacelib

import numpy as np
import time
import datetime

def initPlayers(j, typeIA1, optIA1, vIA1, typeIA2, optIA2, vIA2):
    if typeIA1 == "MinMax" :
        blackPlayer = joueurlib.IA_MinMax(j, "noir", {"Profondeur":optIA1, "Version":1})
    elif typeIA1 == "AlphaBeta" :
        blackPlayer = joueurlib.IA_AlphaBeta(j, "noir", {"Profondeur":optIA1, "Version":vIA1})
    else:
        blackPlayer = joueurlib.IA_Random(j, "noir")

    if typeIA2 == "MinMax" :
        whitePlayer = joueurlib.IA_MinMax(j, "blanc", {"Profondeur":optIA2, "Version":1})
    elif typeIA2 == "AlphaBeta" :
        whitePlayer = joueurlib.IA_AlphaBeta(j, "blanc", {"Profondeur":optIA2, "Version":vIA2})
    else:
        whitePlayer = joueurlib.IA_Random(j, "blanc")
    return [blackPlayer, whitePlayer]

def IAvsIA(occurences, typeIA1, optIA1, vIA1, typeIA2, optIA2, vIA2):
    whiteScores = []
    blackScores = []
    whiteTimeAverageForGames = []
    blackTimeAverageForGames = []
    blackVictories = 0
    whiteVictories = 0
    ties = 0
    whiteScoreAverage = 0
    blackScoreAverage = 0
    whiteElapsedTimesAverage = 0
    blackElapsedTimesAverage = 0
    whiteTotalCalls = []
    blackTotalCalls = []
    whiteTotalCallsAverage = 0
    blackTotalCallsAverage = 0
    totalTimes = []
    averageTime = 0

    for i in range(0, occurences):
        j = Jeu.Jeu(8, {"interface":False, "choix_joueurs":False})
        (black, white) = initPlayers(j, typeIA1, optIA1, vIA1, typeIA2, optIA2, vIA2)
        j.noir = black
        j.blanc = white
        j.joueur_courant = j.noir
        start_time = time.time()
        while not j.partie_finie:
            coup = j.joueur_courant.demande_coup()
            j.jouer(coup)
        totalTimes.append(time.time() - start_time)
        [v, s_noir, s_blanc] = j.score()

        if v == 0:
            ties += 1
        elif v == 1:
            blackVictories += 1
        elif v == -1:
            whiteVictories += 1

        blackScores.append(s_noir)
        whiteScores.append(s_blanc)
        blackTimeAverageForGames.append(np.mean(j.noir.elapsedTimes))
        whiteTimeAverageForGames.append(np.mean(j.blanc.elapsedTimes))

        whiteTotalCalls.append(j.blanc.totalNbAppels)
        blackTotalCalls.append(j.noir.totalNbAppels)

    blackElapsedTimesAverage = np.mean(blackTimeAverageForGames)
    whiteElapsedTimesAverage = np.mean(whiteTimeAverageForGames)
    blackScoreAverage = np.mean(blackScores)
    whiteScoreAverage = np.mean(whiteScores)
    blackTotalCallsAverage = np.mean(blackTotalCalls)
    whiteTotalCallsAverage = np.mean(whiteTotalCalls)
    averageTime = np.mean(totalTimes)

    print("Nombre de victoires de l'IA 1 : " + str(blackVictories) + ".\n")
    print("Nombre de victoires de l'IA 2 : " + str(whiteVictories) + ".\n\n")

    print("Nombre d'égalités ", str(ties), ".\n\n")

    print("Score moyen de l'IA 1 : " + str(blackScoreAverage) + ".\n")
    print("Score moyen de l'IA 2 : " + str(whiteScoreAverage) + ".\n\n")

    print("Temps d'execution moyen de l'IA 1 : " + datetime.datetime.fromtimestamp(blackElapsedTimesAverage).strftime("%M:%S.%f") + " secondes.\n")
    print("Temps d'execution moyen de l'IA 2 : " + datetime.datetime.fromtimestamp(whiteElapsedTimesAverage).strftime("%M:%S.%f") + " secondes.\n\n")

    #Représente le nombre de coups qu'une IA exécutera tout au long de la partie. La moyenne est nécéssaire dans le cas de parties multiples.
    print("Nombre de coups moyen joué à chaque partie par l'IA 1 : " + str(int(blackTotalCallsAverage)) + ".\n")
    print("Nombre de coups moyen joué à chaque partie par l'IA 2 : " + str(int(whiteTotalCallsAverage)) + ".\n")

    print("Temps de partie moyen : " +time.strftime("%H heures, %M minutes et %S secondes", time.gmtime(averageTime)) + ".\n")

IAvsIA(1, "AlphaBeta", 6, 2, "AlphaBeta", 1, 1)