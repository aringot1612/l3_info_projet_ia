import jeulib as Jeu 
import joueurlib
import interfacelib

import numpy as np
import time
import datetime

j = Jeu.Jeu(8, {"interface":False, "choix_joueurs":False})
j.noir = blackPlayer = joueurlib.IA_Random(j, "noir")
j.noir.importData()