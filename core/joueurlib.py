#!/usr/bin/python
# coding: utf8

import interfacelib
import nn as NeuralNetwork
import numpy as np
import random
import time
import datetime

class Joueur:
	def __init__(self, partie, couleur, opts={}):
		self.couleur = couleur
		self.jeu = partie
		self.opts = opts

	def demande_coup(self):
		pass

class Humain(Joueur):
	def demande_coup(self):
		pass

class IA(Joueur):
	bestMove = [0, 0]
	maxDepth = 5
	elapsedTimes = []
	minValue = -1
	maxValue = 100

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)
		self.temps_exe = 0
		self.nb_appels_jouer = 0
		self.totalNbAppels = 0
		self.elapsedTimes = []
		self.bestMove = [0, 0]
		self.minValue = -1
		self.maxValue = 100
		if "Profondeur" not in opts:
			self.maxDepth = 5
		else:
			self.maxDepth = opts["Profondeur"]

	def importData(self):
		(xs, ys) = NeuralNetwork.mnist_to_data('../data/', 'data.csv')
		neuralNetwork = NeuralNetwork.PerceptronMulticouche(64, np.arange(1, 100))
		error = NeuralNetwork.erreur_quadratique(xs, ys, neuralNetwork)
		entro = NeuralNetwork.entropie_croisee(xs, ys, neuralNetwork)
		precision = NeuralNetwork.precision(xs, ys, neuralNetwork)
		print("\nErreur quadratique :", str(error) + "\n")
		print("Entropie croisée :", str(entro) + "\n")
		print("Precision :", str(precision) + "\n")

class IA_Random(IA):
	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)
		self.temps_exe = 0
		self.nb_appels_jouer = 0

	def demande_coup(self):
		if(self.couleur == "noir"):
			coups = self.jeu.plateau.liste_coups_valides(1)
		else:
			coups = self.jeu.plateau.liste_coups_valides(-1)
		if(len(coups) == 0):
			return []

		start_time = time.clock()
		coup = random.choice(coups)
		self.elapsedTimes.append(time.clock() - start_time)
		if hasattr(self.jeu, 'gui'):
			self.jeu.gui.coup = None
		return coup

	def EvalBoard(self, board, player):
		counter = 0
		length = board.taille
		for y in range(length):
			for x in range(length):
				if board.tableau_cases[y][x] == player:
					if (x == 0 or x == length - 1) and (y == 0 or y == length - 1):
						counter += 4 # + 4 points par coins de tableau
					elif (x == 0 or x == length - 1) or (y == 0 or y == length - 1):
						counter += 2 # + 2 points par cases de coté
					else:
						counter += 1 # + 1 point pour une case normale
		return counter

	def IsTerminalNode(self, board, player):
		if(len(board.liste_coups_valides(player)) == 0):
			return True
		else:
			return False

	def getSortedMoves(self, board, player, isMaximizing):
		tmpSortedMoves = []
		sortedMoves = []
		potentialMoves = board.liste_coups_valides(player)
		for move in potentialMoves:
			boardTemp = board.copie()
			boardTemp.jouer(move,player)
			if isMaximizing:
				# Si on cherche à maximiser, il s'agit du joueur principal, on récupère le score selon ce dernier.
				tmpSortedMoves.append([move, self.EvalBoard(boardTemp, player)])
			else:
				# Si on cherche à minimiser, C'est toujours le score du joueur principal qu'il faut retrouver, 
				# mais cela signifie que "player" n'est PAS notre joueur principal.
				tmpSortedMoves.append([move, self.EvalBoard(boardTemp, -player)])
		if isMaximizing:
			tmpSortedMoves.sort(key = lambda x:x[1], reverse=True)
		else:
			tmpSortedMoves.sort(key = lambda x:x[1])
		for i in tmpSortedMoves:
			sortedMoves.append(i[0])
		return sortedMoves

class IA_MinMax(IA_Random):
	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)
		self.temps_exe = 0
		self.nb_appels_jouer = 0
		if "Version" not in opts:
			self.version = 2
		else:
			self.version = opts["Version"]
		
	def demande_coup(self):
		self.nb_appels_jouer = 0
		if(self.couleur == "noir"):
			c = 1
		else:
			c = -1
		coups = self.jeu.plateau.liste_coups_valides(c)
		if(len(coups) == 0):
			return []
		start_time = time.clock()
		self.bestMove = self.MinMax(self.jeu.plateau, c, self.maxDepth, True)
		self.elapsedTimes.append(time.clock() - start_time)
		
		#Représente le nombre de coups que cette IA réalise avant de trouver le meilleur coup possible.
		#  Attention : il est important de commenter la ligne ci-dessous si une partie est créée via run.sh : ce script risque de print ce message dans le csv (nécéssaire pour le graphique)....
		#print("Nombre de coups joués par l'IA Min Max :" + str(self.nb_appels_jouer) + "\r\n")
		#Représente le nombre de coups qu'une IA réalisera au total lors d'une partie.
		self.totalNbAppels += self.nb_appels_jouer
		if hasattr(self.jeu, 'gui'):
			self.jeu.gui.coup = None
		return self.bestMove[0]

	def MinMax(self, board, player, depth, maximizingPlayer):
		opponent = -player
		if (depth < 1):
			return [[0, 0], self.EvalBoard(board, player)]
		if maximizingPlayer:
			if (self.IsTerminalNode(board, player)):
				return [[0, 0], self.EvalBoard(board, player)]
			v = [[0, 0], self.minValue]
			coups = board.liste_coups_valides(player)
			for coup in coups:
				boardTemp = board.copie()
				boardTemp.jouer(coup,player)
				self.nb_appels_jouer += 1
				v_tmp = self.MinMax(boardTemp, player, depth - 1, False)
				if(v_tmp[1] > v[1]):
					v = [coup, v_tmp[1]]
		else:
			if (self.IsTerminalNode(board, opponent)):
				return [[0, 0], self.EvalBoard(board, player)]
			v = [[0, 0], self.maxValue]
			coups = board.liste_coups_valides(opponent)
			for coup in coups:
				boardTemp = board.copie()
				boardTemp.jouer(coup, opponent)
				self.nb_appels_jouer += 1
				v_tmp = self.MinMax(boardTemp, player, depth - 1, True)
				if(v_tmp[1] < v[1]):
					v = [coup, v_tmp[1]]
		return v

class IA_AlphaBeta(IA_MinMax):
	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)
		self.temps_exe = 0
		self.nb_appels_jouer = 0

	def IsTerminalNode(self, board, player):
		for y in range(8):
			for x in range(8):
				if board.est_coup_valide([y,x], player):
					return False
		return True

	def demande_coup(self):
		self.nb_appels_jouer = 0
		if(self.couleur == "noir"):
			c = 1
		else:
			c = -1

		coups = self.jeu.plateau.liste_coups_valides(c)
		if(len(coups) == 0):
			return []
		start_time = time.clock()
		self.bestMove = self.AlphaBeta(self.jeu.plateau, c, self.maxDepth, self.minValue, self.maxValue, True)
		self.elapsedTimes.append(time.clock() - start_time)
		#Représente le nombre de coups que cette IA réalise avant de trouver le meilleur coup possible.
		#  Attention : il est important de commenter la ligne ci-dessous si une partie est créée via run.sh : ce script risque de print ce message dans le csv (nécéssaire pour le graphique)....
		#print("Nombre de coups joués par l'IA Alpha Beta :" + str(self.nb_appels_jouer) + "\r\n")
		#Représente le nombre de coups qu'une IA réalisera au total lors d'une partie.
		self.totalNbAppels += self.nb_appels_jouer

		if hasattr(self.jeu, 'gui'):
			self.jeu.gui.coup = None
		return self.bestMove[0]

	def AlphaBeta(self, board, player, depth, alpha, beta, maximizingPlayer):
		opponent = -player
		if (depth < 1):
			return [[0, 0], self.EvalBoard(board, player)]
		if maximizingPlayer:
			if (self.IsTerminalNode(board, player)):
				return [[0, 0], self.EvalBoard(board, player)]
			v = [[0, 0], self.minValue]
			if(self.version == 1):
				coups = board.liste_coups_valides(player) #VERSION 1
			else:
				coups = self.getSortedMoves(board, player, maximizingPlayer) #VERSION 2
			for coup in coups:
				boardTemp = board.copie()
				boardTemp.jouer(coup,player)
				self.nb_appels_jouer += 1
				v_tmp = self.AlphaBeta(boardTemp, player, depth - 1, alpha, beta, False)
				if(v_tmp[1] > v[1]):
					v = [coup, v_tmp[1]]
				alpha = max(alpha, v[1])
				if(beta <= alpha):
					break
			return v
		else:
			if (self.IsTerminalNode(board, opponent)):
				return [[0, 0], self.EvalBoard(board, player)]
			v = [[0, 0], self.maxValue]
			if(self.version == 1):
				coups = board.liste_coups_valides(opponent) #VERSION 1
			else:
				coups = self.getSortedMoves(board, opponent, maximizingPlayer) #VERSION 2
			for coup in coups:
				boardTemp = board.copie()
				boardTemp.jouer(coup, opponent)
				self.nb_appels_jouer += 1
				v_tmp = self.AlphaBeta(boardTemp, player, depth - 1, alpha, beta, True)
				if(v_tmp[1] < v[1]):
					v = [coup, v_tmp[1]]
				beta = min(beta, v[1])
				if(beta <= alpha):
					break
			return v