import jeulib as Jeu;
import joueurlib
import interfacelib

import sys
import numpy as np
import time
import datetime

filename = "../data/data.csv"

def initPlayers(j, typeIA1, optIA1, vIA1, typeIA2, optIA2, vIA2):
    if typeIA1 == "MinMax" :
        blackPlayer = joueurlib.IA_MinMax(j, "noir", {"Profondeur":optIA1, "Version":1})
    elif typeIA1 == "AlphaBeta" :
        blackPlayer = joueurlib.IA_AlphaBeta(j, "noir", {"Profondeur":optIA1, "Version":vIA1})
    else:
        blackPlayer = joueurlib.IA_Random(j, "noir")

    if typeIA2 == "MinMax" :
        whitePlayer = joueurlib.IA_MinMax(j, "blanc", {"Profondeur":optIA2, "Version:":1})
    elif typeIA2 == "AlphaBeta" :
        whitePlayer = joueurlib.IA_AlphaBeta(j, "blanc", {"Profondeur":optIA2, "Version:":vIA2})
    else:
        whitePlayer = joueurlib.IA_Random(j, "blanc")
    return [blackPlayer, whitePlayer]

def autorun(occurences, depth, alphaBetaVersion):
    blackVictories = 0
    whiteVictories = 0
    ties = 0
    for i in range(0, occurences):
        j = Jeu.Jeu(8, {"interface":False, "choix_joueurs":False})
        (black, white) = initPlayers(j, "AlphaBeta", depth, alphaBetaVersion, "rnd", 0, 0)
        j.noir = black
        j.blanc = white
        j.joueur_courant = j.noir
        while not j.partie_finie:
            coup = j.joueur_courant.demande_coup()
            j.jouer(coup)
        [v, s_noir, s_blanc] = j.score()
        if v == 0:
            ties += 1
        elif v == 1:
            blackVictories += 1
        elif v == -1:
            whiteVictories += 1
    print(str(occurences) + ";" + "AlphaBeta" + ";" + str(depth) + ";" + "Random" + ";" + "0" + ";" + str(blackVictories) + ";" + str(whiteVictories) + ";" + str(ties))

if __name__ == "__main__":
    autorun(int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]))