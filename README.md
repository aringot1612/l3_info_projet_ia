# L3_INFO_Projet_IA

## Explications 

Cette première partie de document présente **les fichiers présents**.

- interfacelib, jeulib et joueurlib : les classes du moteur de jeu Reversi.
- nn.py est un fichier contenant la gestion d'un réseau neuronal Perceptron Multicouche basé sur des données MNIST.
- play.py permet de jouer une partie de Reversi tout en personnalisant les joueurs (Joueur normal ?, IA de type ?, profondeur ?).
- main.py permet de lancer n partie entre deux IA afin de fournir diverses informations : Score moyen, temps d'éxécution moyen, etc...
- autorun.py est un fichier dont le but est de générer n parties d'IA afin de fournir un graphique (ce fichier est appelé par /run/run.sh).
- learning.py est un fichier permettant d'imiter une stratégie d'évolution pour un réseau neuronal Perceptron Multicouche
- mnist_generation.py permet de générer des données MNIST en se basant sur n parties d'IA Random vs Random.
- test_data.py permet de vérifier diverses informations sur le réseau neuronal en se basant sur les données MNIST de Reversi.
- run.sh est un script dont le but est de générer un graphique de comparaison d'IA AlphaBeta vs Random.


## Journal


### 28-06-2020 

- L'IA random a été créée.


### 03-07-2020

- L'IA min-max est implémentée et fonctionnelle.

- L'IA AlphaBeta est implémentée et fonctionnelle.
La différence de temps de calcul est bien ressentie.
Mais des comparaisons de temps de traitement réelles vont être effectuées prochainement.

Afin de préparer les deux algorithmes à être utilisable indépendamment du joueur :

Le min-max utilise désormais l'id du joueur fourni en paramètre, ce simple changement semble avoir engendré un parcours du tableau différent, certainement lié à l'evaluation de coups.

Cette méthode est donc à revoir.

Aussi, il est prévu de rendre la profondeur d'IA paramétrable, afin de gérer plus tard la comparaison d'IA.
(Cela nécéssitera certainement la nouvelle version du jeu)

Il est également prévu d'implémenter l'affichage du temps de calcul dans la console afin de juger de la rapidité d'éxécution des deux algorithmes.


### 05-07-2020


#### Update 9 h 00 :

L'evaluation des coups est désormais adaptée à la comparaison d'IA pour le min max ET l'Alpha Beta.

A profondeur identique, le min-max est bien plus long que l'alpha Beta.

Le jeu a été mis à jour, désormais la comparaison d'IA peut enfin être mise en place.


#### Update 11 h 00 :

L'affichage dans la console du nombres de coups joués par les IA est à présent ajoutée.
En supplément, le temps d'éxécution lors de l'appel aux algorithmes est également ajouté.
Le format choisi est le suivant -> Minutes : Secondes : MilliSecondes.

Cette ajout montre que l'algorithme Aplha Beta est bien plus rapide que le Min Max.


#### Update 11 h 30 :

Le problème suivant : coup retourné différent selon l'algorithme appelé entre Min-Max et Alpha Beta résolu.

Il s'agissait simplement d'une coupure mise en place dans alpha Beta trop tôt.
Certaines branches étaient ignorées sans raison.


#### Update 11 h 50 :

Désormais, il est possible de choisir le type d'IA sans appel à la selection console.


#### Update 12 h 00 :

Le paramètrage d'IA est terminé, il est possible de personnaliser les deux joueurs IA selon leur type et leur profondeur.


#### Update 14 h 00 :

Le programme main.py est désormais capable de simuler une partie entre deux IA.

Différentes comparaisons ont déja été effectués.

Et ces dernières montrent plusieurs choses :
- Les IA MinMax et AlphaBeta générent les mêmes parties à profondeur identique (très bon point) ;
- Une IA Alpha Beta est bien plus efficace qu'une IA MinMax à profondeur identique en terme de vitesse ;
- Une partie de Reversi entre une IA random et une IA AlphaBeta peut donner lieu à des résultats curieux, avec quelques fois des victoires écrasantes de l'IA Random, normalement moins fiable qu'une IA AlphaBeta (à vérifier).


#### Update 17 h 15 :

La comparaison d'IA est améliorée. Elle gère désormais les occurences de parties.
Les résultats sont intéréssants.

Chose curieuse, un ensemble de parties entre une IA Random et une IA AlphaBeta de profondeur 5 est très "aléatoire".
Avec une occurence de 100 parties, l'IA Random est tout de même capable d'en gagner 40~50. Même si l'IA AlphaBeta l'emporte, c'est bien souvent un match très serré.


#### Update 19 h 30 :

La fonction evalBoard a été améliorée.
Elle prend désormais en charge les points selon le type de case possédé par le joueur (coins, cotés ou normales).
Elle compte également les coups possible à l'instant t.

Une fonction getSortedMoves() a été créée afin de récupèrer une liste des coups possibles pré-ajustée selon le score du joueur (en testant tous les coups).
Cette fonction sera utilisée par Alpha Beta version 2.

Prochain objectif :
- Utiliser cette fonction pour l'algorithme Alpha-Beta, afin de juger de son utilité, on espère un traitement des coups encore plus rapide.


#### Update 22 h 15 :

La version Alpha Beta v2 a été testée et les résultats ne sont pas concluants.
Autre chose, l'évaluation de la grille version 2 a semble-t-il augmentée considérablement le temps de calcul des deux algorithmes.

Une optimisation sera certainement nécéssaire.


### 06-07-2020

#### Update 9 h 00 :

Une analyse du code a permis de déceler le problème rencontré hier :
Avec l'ajout d'une méthode d'évaluation plus complexe, le temps de traitement pour les deux algorithmes a considérablement augmenté.
Le temps moyen d'éxécution a été multiplié par 5, voir plus, selon les conditions de jeu, ce qui rend les deux algorithmes Min-Max et AlphaBeta bien plus long.

Visiblement, cette augmentation du temps de calcul provient directement de la méthode liste_coups_valides().
Cette dernière est utilisée dans evalBoard() afin de récupèrer le nombre de coups possible pour le joueur.

Cette ajout à evalBoard permettait de rajouter des points selon le nombre de placements éventuels pour ce joueur.

Afin de réduire le temps de traitement, il a été décidé de temporairement supprimer cet appel à liste_coups_valides().
Par conséquent, la méthode evalBoard() n'utilise que les cases du tableau pour évaluer la grille.

Second point, Après cette modification d'evalBoard() :
Des tests ont été effectués afin de juger de l'utilité de l'alphaBeta version 2.

Pour rappel, nous avons créer une méthode getSortedMoves() dont le but est de classer les placements de pions éventuels selon leur score et de faire appel à ce classement dans AlphaBeta.

Après tests, les résultats sont concluants.
Alpha Beta version 2 est plus rapide qu'Alpha Beta version 1.
Le temps de traitement moyen a diminué de 40% (selon les conditions de test).

**Cela pose néanmoins un problème :**

Après les tests effectués, Alpha Beta version 2 a gagné avec un nombre de pions différents.
Ce qui signifie que l'algorithme ne suit pas de manière identique le cheminement de la première version.

Cela remet en question le traitement effectué par Alpha Beta.


#### Update 11 h 00 :

La cause la plus probable du problème situé plus haut :
La profondeur d'AlphaBeta n'étant pas optimale, Il y a des chances que certaines branches (les derniers coups du classement trié) soient "ignorés" par Alpha Beta en version 2.

A vérifier.


#### Update 12 h 30 :

Ajout d'un fichier run.sh et autorun.py.
Ces fichiers permettent de générer un .csv contenant les expériences AlphaBeta vs Random réalisées sur mesure.

Le fichier run.sh permet de personnaliser :
- La liste des profondeurs d'Alpha Beta testées ;
- le nombre d'occurences de parties pour chaque profondeur testée.


#### Update 15 h 30 :

L'Apprentissage via un essai avec MNIST a été mis en place.
Tout semble fonctionner correctement.

La stratégie d'évolution pour améliorer le réseau neuronal n'a pas encore été implémentée.

Le programme learning.py se contente d'afficher la précision, l'erreur quadratique et les paramètres du perceptron multicouche.

### 07-07-2020

Une première tentative de stratégie d'évolution a été mise en place pour learning.py.

Nous commencons par récupèrer l'erreur quadratique et la précision du réseau neuronal.
Nous récupèrons également ses paramètres.

On va mener l'experience suivante n fois :
- On modifie le paramètre actuel du réseau neuronal avec des valeurs aléatoires (cette méthode sera certainement mise à jour),
- On vérifie la nouvelle précision et la nouvelle valeur d'erreur quadratique,
- Si les deux valeurs sont plus intéréssantes que les meilleurs valeurs précédentes (erreur plus faible et précision plus importante) :
- On garde en mémoire notre paramètre, notre valeur d'erreur quadratique et notre précision : 
  - ces trois valeurs deviennent les "meilleurs valeurs".
- On applique les meilleurs paramètres à notre réseau neuronal.

En théorie, une fois cette éxpérience réalisée, nous obtenons des paramètres optimales : grande précision et faible erreur.

Néanmoins, actuellement, la précision et l'erreur quadratique ne changent jamais.
En d'autres termes, les paramètres par défaut reste les meilleurs.

Cela est certainement lié à la manière de générer les nouveaux paramètres.
Ou bien, les paramètres par défaut sont réellement les meilleurs....

**Update** : La méthode permettant de changer les paramètres a changée, mais aucune amélioration de précision/erreur.


#### Update 16 h 00 :

L'optimisation de notre réseau neuronal a été laissée de coté pendant un moment.

Le déploiement gitlab-ci ne pouvant exécuter un script trop long, le graphique de comparaison d'IA AlphaBeta vs Random sera rajouté plus tard.
Il sera généré depuis la machine de développement.
Par conséquent, le ci-cd gitlab a été supprimé.


En ce qui concerne le réseau neuronal, Nous avons tenté de créer notre propre base de données MNIST.
- Nous avons utilisé autorun.py pour créer 100 parties aléatoires entre deux IA Random.
- A chaque coup porté, nous écrivons dans un fichier csv l'état de la grille de jeu ainsi que le score du joueur noir selon le format suivant : grille de jeu, score
- Nous obtenons le fichier csv fourni ("/data/data.csv").

Il est probable que la manière d'importer nos données dans le fichier csv ne correspond pas à ce que nous souhaitons en faire.

- Ce fichier peut être ouvert par NeuralNetwork.mnist_to_data().
- Le problème :
  - J'ignore pour le moment l'intérêt de ce réseau neuronal et surtout, comment l'utiliser ?

L'importation et le traitement des données mnist sont gérés par la méthode importData() de la classe IA.
Un fichier test_data.py se charge de faire appel à cette méthode importData() afin de vérifier rapidement diverses informations.

Pour générer le fichier data.csv, il faut executer le fichier mnist_generation.py.


### 07-07-2020

#### Update 11 h 15 :

Le fichier nn.py a été légèrement modifié afin de prendre en charge notre utilisation d'une table de données MNIST.
Néanmoins, la question suivante se pose toujours : comment l'utiliser ?

Le format utilisé pour sauvegarder nos données a également changé :
Nous passons du format : <grille de jeu, score> à <score, grille de jeu.>


#### Update 12 h 15 :

Passage de 100 à 1000 parties testées avec des IA Random pour fournir davantage de données mnist.
La tentative de stratégie d'évolution mise en place il y a quelques temps n'a eu aucun effet sur cette expérience.

Malgrès le passage de 100 à 1000 parties, la précision reste extrémement faible (entre 0 et 0.02).
Cela est surement lié au fait q'une grille peut être à l'origine d'un score de 1 à 100.

Je m'interroge sur la manière d'utiliser MNIST, après tout, quel est l'intérêt de prédire un score si il est possible de le calculer très rapidement ?
Je pense changer la manière d'importer nos données dans MNIST mais j'ignore comment.

**Remarque :** Depuis le passage à 1000 parties testées, Je me suis rendu compte que le perceptron multicouche prend en charge nativement le multi-thread. 
Dommage que son utilisation se limite pour le moment à afficher : l'erreur quadratique, l'entropie croisée et la précision.


#### Quelques informations sur le réseau neuronal :

![alt text](pictures/neural_network_stats.png)


### 12-07-2020

#### Update 11 h 00 :

Un premier script a été exécuté afin de réaliser un graphique de comparaison d'IA Alpha Beta vs Random.
Il était impossible de réaliser une telle opération depuis Gitlab CI-CD car cela nécéssite beaucoup de temps.

Le script repose sur le principe suivant,
Dans le fichier run.sh (dossier run) :
- Nous définissons une liste de profondeur pour notre IA principal : Ici, cette liste va de 1 à 7.
- Nous définissons ensuite le nombre de parties à tester pour chaque profondeur (50 par défaut).

Ce script a été lancé une première fois il y a quelques jours, mais avec une profondeur AlphaBeta max égal à 8.
Or avec cette profondeur, une partie pouvait durer plus d'une demi-heure (et bien souvent plus d'une heure).

Le script est donc resté actif pendant plus de 30 heures avant qu'il soit volontairement stoppé.
Il a été possible de créer un premier graphique en se basant sur les 7 premières profondeurs.

![alt text](pictures/out_AlphaBeta_rnd_50.png)

Le script a été lancé une seconde fois, avec une profondeur allant de 1 à 9 et 10 parties par profondeur. (Script exécuté pendant environ 18 heures voir plus.)

![alt text](pictures/out_AlphaBeta_rnd_10.png)


Ces graphiques permettent de se rendre compte que notre algorithme Alpha Beta ne se débrouille pas toujours très bien face à une simple IA Random.

**Petites précisions sur le script initial en se basant sur la première expérience :**

- Une comparaison d'IA AlphaBeta vs Random (7 profondeurs testées en 50 parties) dure environ 9 heures.

**Vis à vis de l'environnement**, le script ne fonctionne que sur un seul coeur CPU à la fois (contrairement au réseau neuronal).

Une haute fréquence de coeur est donc à privilégier sur le nombres de coeurs.
Ici, la fréquence variait selon le coeur chargé du script : entre 4.35 et 4.55 Ghz.


### 13-07-2020

#### Update 18 h 00 :

Les IA Min-Max et AlphaBeta ont été optimisés et corrigés.

**L'une des corrections :**
Auparavant, les deux algorithmes effectuaient une copie de grille de jeu avant de tester tous les coups à leur disposition.
Désormais, la copie est effectuée juste avant chaque coup, évitant ainsi d'effectuer plusieurs coups de même couleur successivement sur la même grille de jeu.

La résolution de ce problème a semble-t-il modifié le comportement des algorithmes.
Dorénavant, lors d'une partie entre deux IA de même type, avoir la profondeur la plus importante ne signifie pas toujours gagner.
J'ignore pour le moment si cela est "normal" ou non.

Voici des graphiques de comparaison d'IA permettant de vérifier la probabilité qu'une IA Alpha Beta à n profondeur gagne face à une IA Random.
Comme nous le remarquons, cette nouvelle version semble très efficace face à une IA Random, même à très faible profondeur.

**Alpha Beta Version 1 :**

![alt text](pictures/out_AlphaBeta2_rnd_v1.png)

**Alpha Beta Version 2 :**

![alt text](pictures/out_AlphaBeta2_rnd_v2.png)


## Reflexions autour de Min-Max, Alpha Beta et le réseau neuronal :


### La fonction d'évaluation :

Cette fonction est très importante pour améliorer le comportement de Min Max et d'AlphaBeta.

Il y a eu trois versions de cette fonction :
1. La toute première se contentait de vérifier le nombre de pions présents dans la grile de jeu selon la couleur donnée en paramètre.
   La valeur ainsi obtenue permettait à Min-Max et AlphaBeta de rechercher le meilleur coup à jouer.
   Les tests pour vérifier cette fonction était très simple, il suffisait de vérifier la valeur retournée pour la comparer à la grille de jeu.

2. La seconde version était bien plus complexe : elle se distingue par deux évaluations spécifiques.
   On ne se contentait plus de compter les pions d'une grille, on leur attribue une valeur selon leur placement.
   Dans le jeu Reversi, gagner une partie n'est pas si simple, l'une des techniques les plus courantes est de chercher à atteindre les coins et les cotés de la grille de jeu :
   Les pions à ces positions sont plus difficile à capturer.
   Ainsi, on va donner l'avantage au joueur possèdant le plus de coins et de cotés.
   Finalement, plutot que d'incrémenter un compteur de 1 à chaque pion de couleur présent.
   L'incrémentation dépendra désormais du placement du pion.

   Seconde ajout permettant d'améliorer cette fonction d'évaluation : Nous allons vérifier le nombre de placement de coups que le joueur concerné est capable de faire.
   Ainsi, une grille de jeu sera mieux notée si le joueur concerné possède un plus grand choix de coup.

3. La dernière version est une version 2 simplifiée :
   L'ajout d'une incrémentation selon le nombre de placement de coups était une bonne idée, 
   mais cette évaluation spécifique a considérablement augmentée le temps de traitement de cette fonction.
   Par conséquent, elle a été tout simplement supprimée.

   Finalement la version finale se contente d'attribuer une valeur à chaque pion selon le placement.


### Le comportement de Min-Max et Alpha Beta :

Le comportement de Min-Max/Alpha Beta n'était pas convaincant au début du projet car il était capable de perdre très facilement contre une IA Random.
Une correction a été apportée à ce problème et cela a grandement amélioré la situation.

Afin de mieux percevoir cette différence, voici deux graphiques de comparaison entre une IA AlphaBeta à profondeur variable contre une IA Random.

Le premier graphique a été créé avant correction du problème :

![alt text](pictures/out_AlphaBeta_rnd_50.png)

Le second après (voir update du 13-07-2020 pour davantage de précision) :

![alt text](pictures/out_AlphaBeta2_rnd_v1.png)


On remarque une nette amélioration, **mais cela va cacher un problème majeur**, si nous effectuons une comparaison entre deux IA de même type et à profondeur variable :

Contrairement à ce que l'on pourrait croire, l'IA bénéficiant d'une profondeur supérieure ne sortira pas toujours gagnant de la partie.
Ce problème n'est toujours pas résolu.

Concernant les statistiques d'execution, le nombre d'appels et le temps d'éxécution ont été vérifiés et cela semble logique.


### Les versions de l'IA AlphaBeta :

On distingue deux versions pour l'IA Alpha Beta.

**la première version** récupère simplement une liste de coups afin de faire les tests nécéssaires pour trouver le meilleur placement (il s'agit du fonctionnement par défaut de l'algorithme)

![alt text](pictures/out_AlphaBeta2_rnd_v1.png)


La seconde version est plus efficace en terme de nombre d'appels, mais perd en précision.

![alt text](pictures/out_AlphaBeta2_rnd_v2.png)

Plutôt que de récupèrer une simple liste, **la version 2** va effectuer un pré-classement des coups selon la notation que ce coup engendre une fois joué sur la grille de jeu.

La notation va se baser sur le joueur principal, celui dont la notation doit être maximisé ou minimisé (il ne s'agit donc pas forcement du joueur portant le coup à l'instant t).

La méthode en charge s'apelle **getSortedMoves()**.
Elle va trier par ordre croissant ou décroissant la liste de coups selon la notation engendrée directement (profondeur 1).

Cette méthode n'est pas parfaite :
Selon la profondeur, cette version 2 s'éloigne de l'algorithme Min-Max.
Pour le moment, impossible de connaitre la raison de ce changement.

On sort les coups de manière croissante ou décroissante selon le joueur en question.

Si le coup analysé par AlphaBeta cherche à maximiser le score du joueur principal, il faut fournir une liste décroissante du score du joueur concerné.

Dans le cas contraire, il faut que le coup soit porté par l'adversaire (celui cherchant à minimiser le score du joueur principal), tout en notant la grille selon le joueur principal lui même (et finir par sortir la liste croissante...)

Cette méthode n'est pas au point :
D'ailleurs, contre certaines IA, la version 2 peut même être plus longue que la version 1 à l'issue de la partie.
C'est pour cette raison qu'il est toujours possible de choisir la version d'AlphaBeta à utiliser.


### Le comportement du nombre d'appels vis à vis du temps passé :

La liaison entre les deux est selon moi très impactée par les performances de la machine et par la profondeur de l'algorithme.
Cela reste une liaison plutôt simple :

- Plus le nombre de coups joués par l'IA augmente, plus le temps d'éxécution augmente également.
- Le temps d'éxécution dépend également de la vitesse max (ou boost) d'un coeur du processeur à l'instant t.
- Le nombre de coups joués dépend de la profondeur : plus la profondeur d'un algorithme est importante, plus il devra effectuer de coups.

L'intérêt de vérifier les deux :
- Le temps d'éxécution est généralement plus parlant que le nombre d'appels mais il dépend en partie de la machine :
  Plus un coeur de processeur cible est "rapide", plus le temps d'éxécution est faible.
  On parle ici de coeur cible, car selon l'environnement matériel et logiciel, le coeur cpu en charge du script IA ne sera pas toujours le "meilleur" coeur.
- Le nombre d'appels est moins parlant mais il s'agit d'une valeur sure : elle peut être utilisée pour comparer plusieurs algorithmes rapidement sans se soucier de la machine.

Voici quelques tests effectués sur les IA AlphaBeta version 1 et 2 permettant de vérifier le nombres d'appels et les temps d'execution :

Ici, nous allons créer une partie simple entre une IA AlphaBeta v1 et v2 bénéficiant d'une profondeur paramétrable, contre une IA AlphaBeta v1 à profondeur fixe à 1.

- AlphaBeta Profondeur 1:
  - Version 1 :
    - Nombre d'appels moyen pour trouver un coup : 196
    - Temps d'execution moyen pour trouver un coup : 0.002176 secondes.
  - Version 2 :
    - Nombre d'appels moyen pour trouver un coup : 196.
    - Temps d'execution moyen pour trouver un coup : 0.002826 secondes.
- AlphaBeta Profondeur 2:
  - Version 1 :
    - Nombre d'appels moyen pour trouver un coup : 916.
    - Temps d'execution moyen pour trouver un coup : 0.012507 secondes.
  - Version 2 :
    - Nombre d'appels moyen pour trouver un coup : 614.
    - Temps d'execution moyen pour trouver un coup : 0.016659 secondes.
- AlphaBeta Profondeur 3:
  - Version 1 :
    - Nombre d'appels moyen pour trouver un coup : 3627.
    - Temps d'execution moyen pour trouver un coup : 0.051413 secondes.
  - Version 2 :
    - Nombre d'appels moyen pour trouver un coup : 2396.
    - Temps d'execution moyen pour trouver un coup : 0.052752 secondes.
- AlphaBeta Profondeur 4:
  - Version 1 :
    - Nombre d'appels moyen pour trouver un coup : 8417.
    - Temps d'execution moyen pour trouver un coup : 0.156914 secondes.
  - Version 2 :
    - Nombre d'appels moyen pour trouver un coup : 6714.
    - Temps d'execution moyen pour trouver un coup : 0.271853 secondes.
- AlphaBeta Profondeur 5:
  - Version 1 :
    - Nombre d'appels moyen pour trouver un coup : 27004.
    - Temps d'execution moyen pour trouver un coup : 0.396067 secondes.
  - Version 2 :
    - Nombre d'appels moyen pour trouver un coup : 18812.
    - Temps d'execution moyen pour trouver un coup : 0.444358 secondes.
- AlphaBeta Profondeur 6:
  - Version 1 :
    - Nombre d'appels moyen pour trouver un coup : 78602.
    - Temps d'execution moyen pour trouver un coup : 1.645397 secondes.
  - Version 2 :
    - Nombre d'appels moyen pour trouver un coup : 35831
    - Temps d'execution moyen pour trouver un coup : 1.365200 secondes.


### Le comportement de l'IA Min-Max contre une IA Random :

De nombreux tests ont été effectués tout le long du projet, et l'IA Random est capable de se débrouiller face à une IA Min Max.
Avec une profondeur située entre 1 et 2, l'IA Random gagne bien plus souvent.
Mais à partir de la profondeur 3, la tendance s'inverse : l'IA Min Max gagne plus souvent que l'IA Random.....

### Le comportement de l'IA Alpha-Beta contre une IA Random :

....Mais récemment, la plupart des tests effectués se concentrent sur l'IA Alpha Beta contre l'IA Random. (l'IA Min-Max étant très lente).

Les graphiques présent ci-dessous montrent la probabilité qu'une IA Alpha Beta gagne face à une IA Random selon sa profondeur :

**Version 1 d'AlphaBeta**

![alt text](pictures/out_AlphaBeta2_rnd_v1.png)

**Version 2 d'AlphaBeta**

![alt text](pictures/out_AlphaBeta2_rnd_v2.png)

Nous remarquons que les IA Alpha Beta gagnent très facilement contre une IA random.

### Le comportement de deux IA Min Max de profondeur différentes :

- Min Max **1** vs Min Max **1** - > l'IA 2 gagne
- 
- Min Max **1** vs Min Max **2** - > l'IA 2 gagne
- Min Max **2** vs Min Max **1** - > l'IA 2 gagne
- 
- Min Max **2** vs Min Max **3** - > l'IA 2 gagne
- Min Max **3** vs Min Max **2** - > l'IA 1 gagne-
- 
- Min Max **3** vs Min Max **4** - > l'IA 1 gagne
- Min Max **4** vs Min Max **3** - > l'IA 2 gagne
- 
- Min Max **4** vs Min Max **5** - > l'IA 1 gagne
- Min Max **5** vs Min Max **4** - > l'IA 1 gagne

### Le comportement de deux IA Alpha Beta de profondeur différentes :

**Alpha Beta version 1**
- Alpha Beta **1** vs Alpha Beta **1** - > l'IA 2 gagne
- 
- Alpha Beta **1** vs Alpha Beta **2** - > l'IA 2 gagne
- Alpha Beta **2** vs Alpha Beta **1** - > l'IA 2 gagne
- 
- Alpha Beta **2** vs Alpha Beta **3** - > l'IA 2 gagne
- Alpha Beta **3** vs Alpha Beta **2** - > l'IA 1 gagne
- 
- Alpha Beta **3** vs Alpha Beta **4** - > l'IA 1 gagne
- Alpha Beta **4** vs Alpha Beta **3** - > l'IA 2 gagne
- 
- Alpha Beta **4** vs Alpha Beta **5** - > l'IA 1 gagne
- Alpha Beta **5** vs Alpha Beta **4** - > l'IA 1 gagne

**Ensuite, avec l'alpha Beta v2 :**
- Alpha Beta **1** vs Alpha Beta **1** - > l'IA 2 gagne
- 
- Alpha Beta **1** vs Alpha Beta **2** - > l'IA 2 gagne
- Alpha Beta **2** vs Alpha Beta **1** - > l'IA 2 gagne
- 
- Alpha Beta **2** vs Alpha Beta **3** - > l'IA 2 gagne
- Alpha Beta **3** vs Alpha Beta **2** - > l'IA 1 gagne
- 
- Alpha Beta **3** vs Alpha Beta **4** - > l'IA 1 gagne
- Alpha Beta **4** vs Alpha Beta **3** - > l'IA 2 gagne
- 
- Alpha Beta **4** vs Alpha Beta **5** - > l'IA 2 gagne
- Alpha Beta **5** vs Alpha Beta **4** - > l'IA 1 gagne

Les situations montrées plus haut sont parfois bien étrange : Avoir une profondeur supérieure à l'autre ne garantit pas la victoire.
J'ignore pour le moment si il s'agit d'un comportement normal ou non.


### Incertitudes :

La plus grande incertitude concerne directement le point expliqué plus haut.
La version 2 d'Alpha Beta est plus rapide, mais en classant les coups, on se rend compte que l'algorithme est également plus incertain.

**La raison est quelque peu obscure.**
Selon moi, cela vient du fait qu'Alpha Beta va ignorer des branches plus rapidement car à une profondeur faible, elle ne pourra pas anticiper précisément la fin de partie.

En pré-classant les coups, je la rend plus "fainéante" à faible profondeur car le pré-classement donnera une vision très précoce du jeu.
Ainsi, l'algorithme trouvera un coup rapidement mais ce coup est engendré par une faible anticipation.

J'ignore comment améliorer la situation, d'où le choix de laisser la version 1 à disposition.


**Autre chose**, concernant la version 1 d'Alpha Beta et Min-Max, on se rend compte qu'il y a quelques incertitudes.
Des situations où une profondeur supérieure à l'adversaire ne signifie pas "gagner".

J'ignore encore une fois pourquoi certaines situations semblent incorrectes.

**Et enfin**, le perceptron mutlicouche,
Cette dernière partie a été commencée mais elle n'est pas au point.
Malgrès plusieurs tentatives, impossible de comprendre clairement comment utiliser le perceptron multi-couche dans notre cas.

Il y a plusieurs questions.
- A quoi ce réseau neuronal doit servir exactement ?
  - Doit-il trouver le score selon la grille de jeu ? Si oui, pourquoi faire appel à une méthode aussi lourde si le calcul est par défaut très rapide ?
  - Doit-il trouver le score de fin de partie en se basant sur une grille de jeu quelconque ? Si oui, comment ? Quel est le format à utiliser pour les données mnist correspondantes ?
- Comment l'utiliser ?
  - Les méthodes présentes dans le fichier nn.py servent selon moi davantage à tester les capacités d'un réseau, mais quelle fonction permet de l'utiliser en conditions réelles ?
- Comment les paramètres du réseau neuronal fonctionnent ? 
  - En y accèdant via get_params, on obtient une liste de paramètre conséquente.
  - Afin de mettre en place une stratégie d'évolution, j'ai tenté de modifier cette liste via des valeurs reprennant la stratégie d'évolution vue en TP.
    - Mais en finalité, toute modificiation rendait le réseau neuronal moins précis.


### Remarque :

Au debut de ce README, nous retrouvons une liste de fichiers.
Parmis eux, run.sh permet de générer n parties, mais ce fichier a pour but premier de créer des graphiques.

Les valeurs utilisées pour créer nos graphiques sont placées dans un fichier csv.
Mais pour placer les valeurs en question, il a été décidé de faire appel à un opérateur de sortie, allant de la sortie console standard vers le fichier csv.

Par conséquent, il est préférable de sauvegarder dans le fichier csv que les valeurs utiles.
Par extension, cela signifie que la console ne doit afficher **QUE** les valeurs utiles (il n'y a pas de filtrages lors de l'utilisation de l'opérateur de sortie).

Si, par exemple, le nombre de coups joués par les IA se retrouve dans la console, cette valeur (chaine de caractère) se retrouvera dans le fichier csv et cela risque de rendre la création du graphique impossible.

Ainsi, pour éviter ce problème, l'affichage du nombre de coups joués est commenté par défaut.
Si le jeu n'est pas lancé par run.sh, il est possible de dé-commenter les lignes en question afin de retrouver les valeurs affichées dans la console.
Les lignes concernés dans le fichier **joueurlib.py** : ligne 143 et ligne 208.