#!/bin/sh

CMP_BIN="../core/autorun.py"
NSIMSS="1 2 3 4 5 6 7"
NRUNS=50
CSVFILEV1="out_AlphaBeta2_rnd_v1.csv"
PNGFILEV1="out_AlphaBeta2_rnd_v1.png"

CSVFILEV2="out_AlphaBeta2_rnd_v2.csv"
PNGFILEV2="out_AlphaBeta2_rnd_v2.png"

rm -f ${CSVFILEV1}
for nsims in ${NSIMSS} ; do
    echo ""
    echo "is running -> Profondeur d'Alpha Beta = ${nsims}"
    /usr/bin/python3 ${CMP_BIN} ${NRUNS} ${nsims} 1 >> ${CSVFILEV1}
    /usr/bin/python3 ${CMP_BIN} ${NRUNS} ${nsims} 2 >> ${CSVFILEV2}
done

gnuplot -e "set out '${PNGFILEV1}' ; \
    set terminal png size 640,360 ; \
    set datafile separator ';' ; \
    set style data linespoints ; \
    set grid xtics ytics ; \
    set yrange [0:1] ; \
    set xlabel 'AlphaBeta depth' ; \
    set ylabel '% AlphaBeta wins' ; \
    set key bottom right ; \
    plot '${CSVFILEV1}' using 3:(\$6/${NRUNS}) title 'AlphaBeta vs Random' lw 2 "

gnuplot -e "set out '${PNGFILEV2}' ; \
    set terminal png size 640,360 ; \
    set datafile separator ';' ; \
    set style data linespoints ; \
    set grid xtics ytics ; \
    set yrange [0:1] ; \
    set xlabel 'AlphaBeta depth' ; \
    set ylabel '% AlphaBeta wins' ; \
    set key bottom right ; \
    plot '${CSVFILEV2}' using 3:(\$6/${NRUNS}) title 'AlphaBeta vs Random' lw 2 "